# coding=utf-8
from django import forms, contrib
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from core.models import CompanyLine, Company, UserProfile

__author__ = 'idursun'

class CompanyLineForm(forms.ModelForm):
    borc_tam = forms.IntegerField(min_value=0)
    borc_kusur = forms.IntegerField(min_value=0, max_value=100)
    alacak_tam = forms.IntegerField(min_value=0)
    alacak_kusur = forms.IntegerField(min_value=0, max_value=100)
    created_at = forms.DateField(input_formats=('%d/%m/%Y','%d-%m-%Y',))

    class Meta:
        model = CompanyLine
        exclude = ('created_by','modified_by', 'modified_at', 'company', 'borc', 'alacak',)


class CompanyForm(forms.ModelForm):
    limit_tam = forms.IntegerField(min_value=0)
    limit_kusur = forms.IntegerField(min_value=0, max_value=99)

    class Meta:
        model = Company
        exclude = ('limit','owner',)

class UserCreateForm(contrib.auth.forms.UserCreationForm):
    email = forms.EmailField()
