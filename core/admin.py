from django.contrib import admin
from core.models import Company, CompanyLine, UserProfile

__author__ = 'idursun'

admin.site.register(Company)
admin.site.register(CompanyLine)
admin.site.register(UserProfile)

